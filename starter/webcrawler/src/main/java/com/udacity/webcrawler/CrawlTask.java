package com.udacity.webcrawler;

import com.udacity.webcrawler.parser.PageParser;
import com.udacity.webcrawler.parser.PageParserFactory;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.RecursiveAction;
import java.util.regex.Pattern;

public class CrawlTask extends RecursiveAction {
    private final Clock clock;
    private final Duration timeout;
    private final String url;
    private final Instant deadline;
    private final int maxDepth;
    private final PageParserFactory parserFactory;
    private final ConcurrentMap<String, Integer> counts;
    private final ConcurrentSkipListSet<String> visitedUrls;
    private final List<Pattern> ignoredUrls;

    public CrawlTask(Clock clock, Duration timeout, String url, Instant deadline, int maxDepth, PageParserFactory parserFactory, ConcurrentMap<String, Integer> counts, ConcurrentSkipListSet<String> visitedUrls, List<Pattern> ignoredUrls) {
        this.clock = clock;
        this.timeout = timeout;
        this.url = url;
        this.deadline = deadline;
        this.maxDepth = maxDepth;
        this.parserFactory = parserFactory;
        this.counts = counts;
        this.visitedUrls = visitedUrls;
        this.ignoredUrls = ignoredUrls;
    }

    @Override
    protected void compute() {
        if (maxDepth == 0 || clock.instant().isAfter(deadline)) {
            return;
        }
        for (Pattern pattern : ignoredUrls) {
            if (pattern.matcher(url).matches()) {
                return;
            }
        }
        if (visitedUrls.contains(url)) {
            return;
        }
        visitedUrls.add(url);
        PageParser.Result result = parserFactory.get(url).parse();
        for (Map.Entry<String, Integer> e : result.getWordCounts().entrySet()) {
            if (counts.containsKey(e.getKey())) {
                counts.put(e.getKey(), e.getValue() + counts.get(e.getKey()));
            } else {
                counts.put(e.getKey(), e.getValue());
            }
        }
        List<CrawlTask> crawlTasks = new ArrayList<>();
        for (String link : result.getLinks()) {
            crawlTasks.add(new CrawlTask (clock, timeout, link, deadline, maxDepth - 1, parserFactory, counts, visitedUrls, ignoredUrls));
        }
        invokeAll(crawlTasks);
    }
}
